class Accumulator:

    @staticmethod
    # part 1
    def get_accumulator(data):
        accumulator = 0
        line = 0
        instructions = []

        # we don't want to get to the same line again as said in the instructions
        while line not in instructions:
            instructions.append(line)

            # get our first instructions and update the list everytime we traverse through the loop
            current_instruction = data[line].split()
            command, number = current_instruction[0], current_instruction[1]

            if "+" in number: # from list position is one
                number = int(number[1:]) # avoid the first character which is the plus sign
            else:
                number = int(number)

            if command == "acc":
                accumulator += number
                line += 1
            elif command == "jmp":
                line += number
            elif command == "nop":
                line += 1

        return accumulator

    @staticmethod
    def get_accumulator_ending(data):
        accumulator = 0
        line = 0
        instructions = []

        # we don't want to get to the same line again as said in the instructions
        while line not in instructions:
            instructions.append(line)

            # get our first instructions and update the list everytime we traverse through the loop
            current_instruction = data[line].split()
            command, number = current_instruction[0], current_instruction[1]

            if "+" in number: # from list position is one
                number = int(number[1:]) # avoid the first character which is the plus sign
            else:
                number = int(number)

            if command == "acc":
                accumulator += number
                line += 1
            elif command == "jmp":
                line += number
            elif command == "nop":
                line += 1

            if line >= len(data):
                return accumulator, True

        return accumulator, False

