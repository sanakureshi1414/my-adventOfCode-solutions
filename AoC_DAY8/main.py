# AdventOfCodeDay8
# TO-DO: Run your copy of the boot code. Immediately before any instruction is executed a second time,
# what value is in the accumulator?
# TO-DO: Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp).
# What is the value of the accumulator after the program terminates?

from Accumulator import Accumulator


def main():
    with open("Accumulator ") as file:
        listOfInstructions = [line.strip() for line in file]

    print("----Part One----")
    print("Value of the accumulator: " + str(Accumulator.get_accumulator(listOfInstructions)) + "\n")

    for i in range(len(listOfInstructions)):
        if "jmp" in listOfInstructions[i]:
            listOfInstructions[i] = listOfInstructions[i].replace("jmp", "nop")
            acc, found = Accumulator.get_accumulator_ending(listOfInstructions)

            if found:
                print("----Part Two----")
                print("Value of accumulator after program terminates: " + str(acc))

                break
            else:
                listOfInstructions[i] = listOfInstructions[i].replace("nop", "jmp")


main()

# # part 1
# def get_accumulator():
#     accumulator = 0
#     line = 0
#     instructions = []
#
#     # we don't want to get to the same line again as said in the instructions
#     while line not in instructions:
#         instructions.append(line)
#
#         # get our first instructions and update the list everytime we traverse through the loop
#         current_instruction = listOfInstructions[line]
#         current_instruction = current_instruction.split()  # parse the command and values into a list
#         command = current_instruction[0]
#         number = current_instruction[1]
#
#         if "+" in number: # from list position is one
#             number = int(number[1:]) # avoid the first character which is the plus sign
#         else:
#             number = int(number)
#
#         if command == "acc":
#             accumulator += number
#             line += 1
#         elif command == "jmp":
#             line += number
#         elif command == "nop":
#             line += 1
#
#     return accumulator
#
# def get_accumulator_ending():
#     acc = 0
#     line = 0
#     instructions = []
#
#     # we don't want to get to the same line again as said in the instructions
#     while line not in instructions:
#         instructions.append(line)
#
#         # get our first instructions and update the list everytime we traverse through the loop
#         current_instruction = listOfInstructions[line]
#         current_instruction = current_instruction.split()  # parse the command and values into a list
#         command = current_instruction[0]
#         number = current_instruction[1]
#
#         if "+" in number: # from list position is one
#             number = int(number[1:]) # avoid the first character which is the plus sign
#         else:
#             number = int(number)
#
#         if command == "acc":
#             acc += number
#             line += 1
#         elif command == "jmp":
#             line += number
#         elif command == "nop":
#             line += 1
#
#         if line >= len(listOfInstructions):
#             return acc, True
#
#     return acc, False
#
#
# print("----Part One----")
# print("Value of the accumulator: " + str(get_accumulator()) + "\n")
#
#
# for i in range(len(listOfInstructions)):
#     if "jmp" in listOfInstructions[i]:
#         listOfInstructions[i] = listOfInstructions[i].replace("jmp", "nop")
#         acc, found = get_accumulator_ending()
#
#         if found:
#             print("----Part Two----")
#             print("Value of accumulator after program terminates: " + str(acc))
#
#             break
#         else:
#             listOfInstructions[i] = listOfInstructions[i].replace("nop", "jmp")
#


