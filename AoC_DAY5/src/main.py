# AdventOfCodeDay5
# TO-DO: What is the highest seat ID on a boarding pass?

from PassportInfo import PassportInfo

def main():
    with open("data/BoardingPasses") as file:
        listOf_BoardingPasses = [line.strip() for line in file]

        largest = 0
        ids = []

        for boardingPass in listOf_BoardingPasses:
            row = PassportInfo.get_row(None, boardingPass[:7])
            column = PassportInfo.get_column(None, boardingPass[7:])

            seat_id = row * 8 + column

            if seat_id > largest:
                largest = seat_id

            ids.append(seat_id)

            for seat_id in ids:
                if seat_id + 1 not in ids and seat_id + 2 in ids:
                    missing = seat_id + 1

    print("----Part One----")
    print("Highest seat ID on a boarding pass: " + str(largest) + "\n")

    print("----Part Two----")
    print("Id of your seat: " + str(missing))

main()

