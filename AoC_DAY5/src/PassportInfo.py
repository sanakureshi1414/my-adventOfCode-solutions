class PassportInfo():
    def get_row(self, data, lower=0, upper=127):
        for i in range(6):
            half = (upper + lower) // 2
            if data[i] == "F":
                upper = half

            elif data[i] == 'B':
                lower = half + 1

        if data[6] == 'F':
            return lower
        else:
            return upper

    def get_column(self, data, lower=0, upper=7):
        for i in range(2):
            half = (upper + lower) // 2
            if data[i] == 'L':
                upper = half

            elif data[i] == 'R':
                lower = half + 1

        if data[2] == 'L':
            return lower
        else:
            return upper
