*This is a outline of each day of the challenge listing the day, code overview, and level of difficulty.*

| Date       | Code Overview: New/Revised Content                                 |Level Of Difficulty
|------------|--------------------------------------------------------------------|--------------------
| Day 1      | opening files, functions,  methods                                 | EASY                              
| Day 2      | opening files, functions, keeping track of variables               | EASY 
| Day 3      | pratice organizing code into different directories                 | EASY 
| Day 4      | classes, functions, code refactoring                               | HARD 
| Day 5      | functions, code review, recursive functions                        | MEDIUM
| Day 6      | reveiw of coding with lists and dictionaries                       | MEDIUM
| Day 7      | recursive functions, code refactoring                              | EASY - MEDIUM 
| Day 8      | classes, organization of code, functions, static and class methods | HARD 
| Day 9      | refactoring code, recursive functions                              | HARD
| Day 10     | testing code, practice with unit tests, functions                  | MEDIUM
