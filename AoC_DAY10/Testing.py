import unittest


class MyTestCase(unittest.TestCase):
    def test_partOne(self):
        countOne = 72
        countTwo = 33
        result = countOne * countTwo
        self.assertEqual(result, 2376)


    def test_partTwo(self):
        total_count = 129586085429248
        self.assertEqual(total_count, 129586085429248)


if __name__ == '__main__':
    unittest.main()
