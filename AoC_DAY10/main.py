# AdventOfCodeDay10
# TO-DO: What is the number of 1-jolt differences multiplied by the number of 3-jolt differences?

with open("Joltage") as file:
    listOfJoltage = [int(line.strip()) for line in file]
    listOfJoltage.sort()

listOfJoltage = [0] + listOfJoltage
listOfJoltage.append(max(listOfJoltage) + 3)

# part 1
def get_difference_count():
    countOne = 0
    countThree = 0

    for i in range(len(listOfJoltage) - 1):
        difference = listOfJoltage[i + 1] - listOfJoltage[i]

        if difference == 1:
            countOne += 1     # keep track of the number of 1 jolts
        elif difference == 3:
            countThree += 1   # keep track of the number of 3 jolts

    difference_count = countOne * countThree

    return difference_count

# part 2 - recursive function
checked = {}
def get_ways_count(position):
    # base case
    if position == len(listOfJoltage) - 1:
        return 1

    if position in checked:
        return checked[position]

    total_ways = 0
    for i in range(position + 1, len(listOfJoltage)):
        if listOfJoltage[i] - listOfJoltage[position] <= 3:
            total_ways += get_ways_count(i) # goes all the way to the end, checks the number of ways and keeps going until base case if reached

    checked[position] = total_ways

    return total_ways


def main():
    print("----Part One----")
    print("Number of 1 jolt different * Number of 3 jolt differences: " + str(get_difference_count()) + "\n")

    print("----Part Two-----")
    print("Total number of ways: " + str(get_ways_count(0)))


main()


