# AdventOfCodeDay9
# TO-DO: What is the first number that does not have this property from the description given?
# CREDIT TO "DYLAN CODES" Youtube channel for some parts

with open("Numbers") as file:
    listOfNumbers = [int(line.strip()) for line in file]

# get the bad number from the list given: bad number is which adds to a number
def partOne():
    for i in range(25, len(listOfNumbers)):
        numbers, number = listOfNumbers[i - 25:i], listOfNumbers[i]

        found = False   # use a flagger here

        # nested loop to iterate through the numbers list and find bad number
        for j in range(len(numbers) - 1):
            for k in range(j + 1, len(numbers)):
                # this is where we check for the bad number, if following condition is true
                if numbers[j] + numbers[k] == number:
                    found = True
                    break

        # if we find a valid number we need to continue to the next value of i
        if found:
            continue

        # return the number we found
        return number


def partTwo():
    # get the bad number from part one
    bad_number = partOne()
    found = False

    for i in range(len(listOfNumbers) - 1):
        numbers = [listOfNumbers[i]]               # create numbers list
        for j in range(i + 1, len(listOfNumbers)):
            numbers.append(listOfNumbers[j])

            if sum(numbers) == bad_number:
                found = True
                break
            elif sum(numbers) > bad_number:       # we need to start over again, at this point found is FALSE
                break

        if found:
            break   # break out of the loop of the number if found and return the result as a sum

    sum_of_numbers = min(numbers) + max(numbers)

    return sum_of_numbers


def main():
    print("----Part One----")
    print("Number that does not have property: " + str(partOne()) + "\n")

    print("----Part Two----")
    print("Encrytion weakness of XMAS-encrypted list of numbers: " + str(partTwo()))


main()







