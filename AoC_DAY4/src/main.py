# AdventOfCodeDay4
# TO-DO: Find how many passports are valid from the batch file
# TO-D0: Count the number of valid passports by checking if information is in the correct range

from Passport import Passport
from Validator import Validator

def main():
    with open("/users/sanakureshi/Desktop/AOC_DAY4/data/Passports") as file:
        listOfpassports = Passport.get_passports(file)
        validators = [Validator(passport) for passport in listOfpassports]

        count_part1 = 0
        count_part2 = 0

        for validator in validators:
            if validator.check_field_count():
                count_part1 += 1
            if validator.is_valid():
                count_part2 += 1

    print("----Part One----")
    print("Valid Passports: " + str(count_part1) + "\n")

    print("----Part Two----")
    print("Valid Passports with validation: " + str(count_part2))

main()
