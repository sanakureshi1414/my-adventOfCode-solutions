class Passport(object):

    @staticmethod
    def get_passports(data):

        passports = []
        passport = {}

        for line in data:
            if line != "\n":
                line = line.rstrip().split(" ")
                line = [field.split(":") for field in line]

                for field in line:
                    passport[field[0]] = field[1]

            else:
                passports.append(passport)
                passport = {}

        passports.append(passport)

        return passports

