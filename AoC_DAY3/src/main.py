# AdventOfCodeDay3
# TO-D0: How many tree's would you encounter as you traverse with a slope of right 3 and down 1
# TO-DO: Find how many trees are there in all slopes given in the question

with open("/users/sanakureshi/Desktop/AOC_DAY3/data/Map") as file:
    treeMap = [line[:-1] for line in file]

# part 1
def slope(right, down):
    x, y = 0, 0
    count = 0
    widthAtPositionX = len(treeMap[0])

    while not y >= len(treeMap) - 1:
        x += right
        y += down

        # note to myself: visualize this as this as a a gride graph where the x axis is the width when you are graphing
        if x >= widthAtPositionX:
            x -= widthAtPositionX

        # note:don't do [x][y], results in an index out of bounds error
        if treeMap[y][x] == "#":
            count += 1

    return count

# part 2
def allSlopes():
    trees = slope(1, 1) * slope(3, 1) * slope(5, 1) * slope(7, 1) * slope(1, 2)
    return trees

# OUTPUT
def main():
    print("----Part One----")
    print("Tree Count for Slope(3, 1): " + str(slope(3, 1)) + "\n")

    print("----Part Two----")
    print("Tree Count for All Slopes: " + str(allSlopes()))

main()


