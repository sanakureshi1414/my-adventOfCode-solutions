# AdventOfCodeDay7
# TO-DO: How many bag colors can eventually contain at least one shiny gold bag?
# CREDITS TO "DYLAN CODES" Youtube Channel

with open("Baggage ") as file:
    listOfBaggage = [line.strip() for line in file]

# part 1 (recursive function)
def get_num_bags(color):
    # this will return the first index in the string, if that is not equal o 0 then the color is not at the beginning
    lines = [line for line in listOfBaggage if color in line and line.index(color) != 0]

    allColors = []

    # base case for recursive function
    if len(lines) == 0: # no colors hold a bag so we return a empty list
        return []
    else:
        # take the line and take the substring slice of that and we only want up to the index of "bags"
        colors = [line[:line.index(" bags ")] for line in lines]

        # we only need to check for colors that have not been checked before
        colors = [color for color in colors if color not in allColors]

        for color in colors:
            allColors.append(color)
            bags = get_num_bags(color) # this is where we call the recursive function with the color

            allColors += bags

        # check for duplicates here
        unique_colors = []
        for color in allColors:
            if color not in unique_colors:
                unique_colors.append(color)

        return unique_colors

# part 2
def get_bag_count(color):
    rule = ""

    for line in listOfBaggage:
        if line[:line.index(" bags ")] == color: # if ist at the beginning
            rule = line

    if "no" in rule:
        return 1
    # cuts the string to get something specific, make into a list by split method
    rule = rule[rule.index("contain") + 8:].split()

    total = 0
    i = 0
    while i < len(rule):
        count = int(rule[i])
        color = rule[i + 1] + " " + rule[i + 2]

        total += count * get_bag_count(color)

        i += 4

    return total + 1


# part 1 - output
print("----Part One----")
print("Bags that contain at least one shiny gold: " + str(len(get_num_bags("shiny gold"))) + "\n")

# part 2 - output
# solution for this is 38427 - 1 = 38426
print("----Part Two----")
print("Individual bags are required inside your single shiny gold bag: " + str(get_bag_count("shiny gold")))


