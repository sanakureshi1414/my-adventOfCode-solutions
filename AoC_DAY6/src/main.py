# AdventOfCodeDay6
# TO-DO: What is the sum of unique answers count in the data given to you?
# TO-DO: What is the sum of ALL answers in the data given to you?

# part 1
def unique_answers(responses):
    questions = []                     # empty list to append questions that are unique (YES responses)

    for char in responses:             # response is going to be a long string that comes from the data list in main
        if char not in questions:
            questions.append(char)     # only add char to list if it hasn't been seen yet

    return len(questions)              # getting the length of the questions list will return all unique answers we have


# part 2
def all_answers(responses):
    questions = []

    for char in responses[0]:       # we just need to check the characters in the first line, if not in first line [0]
        all_lines = True            # we will assume every satisfies the condition
        for line in responses:
            if char not in line:    # if the char is not in the line then we return false
                all_lines = False

        if all_lines and char not in questions:     # check is no duplicates
            questions.append(char)                  # append to the list

    return len(questions)

def main():
    with open("data/Questions") as file:
        data = [line.strip() for line in file]

    # part 1 and part 2 main functionality is done here
    part1_sum = 0                     # initialize sum to a starting value of 0
    part1_current_response = ""       # this is for the current log of items in the list
    part2_sum = 0                     # same as above ^^^
    part2_current_response = []       # empty list to append the number of all questions, so each line is separated into list

    for line in data:
        # check if we are still in the same block of text then we can add the line to the current response list
        if line != "":
            part1_current_response += line
            part2_current_response.append(line)
        else:
            part1_sum += unique_answers(part1_current_response)     # count how many unique answers we have
            part1_current_response = ""                             # do this, otherwise we would be testing the same line again

            part2_sum += all_answers(part2_current_response)        # count how many answers we have in total
            part2_current_response = []                             # do this, otherwise program will look at the same line again

    part1_sum += unique_answers(part1_current_response)
    part2_sum += all_answers(part2_current_response)

    print("----Part One----")
    print("Number of unique answers: " + str(part1_sum) + "\n")

    print("----Part Two----")
    print("Number of all answers: " + str(part2_sum))

main()







